class RPNCalculator
  # TODO: your code goes here!
  # attr_accessor :calculator

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def value
    @stack.last
  end

  def plus
    @stack << calculator_operation(:+)
  end

  def minus
    @stack << calculator_operation(:-)
  end

  def divide
    @stack << calculator_operation(:/)
  end

  def times
    @stack << calculator_operation(:*)
  end

  def tokens(string)
    tokens = string.split
    tokens.map {|char| operation?(char)? char.to_sym : Integer(char)}
  end

  def evaluate(string)
    token = tokens(string)
    # $stderr.puts token
    token.each do |el|
      case el
      when Integer
        push(el)
      else
        push(calculator_operation(el))
      end
    end

    value
  end

  private

  def operation?(char)
    [:+, :-, :*, :/].include?(char.to_sym)
  end

  def calculator_operation(symbol)
    raise "calculator is empty" if @stack.size < 2

    second_operand = @stack.pop
    first_operand = @stack.pop

    case symbol

    when :+
      first_operand + second_operand
    when :-
      first_operand - second_operand
    when :*
      first_operand * second_operand
    when :/
      first_operand.to_f / second_operand.to_f
    end

  end

end
